package eu.righettod.graphqlpoc.types;

/**
 * Hold the data of a CatResolver.
 */
public class Cat {

    private int id;
    private String name;
    private Person person;

    public Cat(int id, String name, Person person) {
        this.id = id;
        this.name = name;
        this.person = person;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @Override
    public String toString() {
        return "CatResolver{" +
                       "id=" + id +
                       ", name='" + name + '\'' +
                       ", person=" + person +
                       '}';
    }
}
