package eu.righettod.graphqlpoc.types;

import java.util.List;

/**
 * Hold the data of a Person.
 */
public class Person {

    //[VULN]: There an IDOR issue here because the identifier are simple sequential integer
    private int id;

    private String name;
    private List<Cat> cats;

    //[VULN]: There an sensitive access issue here because the data mapping expose the "popularity" storage field and this one must not be seen by GraphQL client
    private int popularity;


    public Person(int id, String name, int popularity, List<Cat> cats) {
        this.id = id;
        this.name = name;
        this.popularity = popularity;
        this.cats = cats;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getPopularity() {
        return popularity;
    }

    public List<Cat> getCats() {
        return cats;
    }

    @Override
    public String toString() {
        return "Person{" +
                       "id=" + id +
                       ", name='" + name + '\'' +
                       ", cats=" + cats +
                       ", popularity=" + popularity +
                       '}';
    }
}
