package eu.righettod.graphqlpoc.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import eu.righettod.graphqlpoc.repository.BusinessDataRepository;
import eu.righettod.graphqlpoc.types.Cat;
import eu.righettod.graphqlpoc.types.Person;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Resolve Veterinary object from a Cat object.
 */
public class CatResolver implements GraphQLResolver<Cat> {

    /**
     * Accessor to business data
     */
    @Autowired
    private BusinessDataRepository businessDataRepository;

    /**
     * Resolve the Person instance from a Cat instance
     *
     * @param d Cat instance
     * @return The Person instance for the Cat instance passed
     * @throws Exception if any error occurs
     */
    public Person person(Cat d) throws Exception {
        Cat dg = businessDataRepository.findCatById(d.getId());
        return dg != null ? dg.getPerson() : null;
    }
}
