package eu.righettod.graphqlpoc.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import eu.righettod.graphqlpoc.repository.BusinessDataRepository;
import eu.righettod.graphqlpoc.security.AccessTokenManager;
import eu.righettod.graphqlpoc.types.Cat;
import eu.righettod.graphqlpoc.types.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Resolvers for all Queries defined in the Schema.
 */
@Component
public class Query implements GraphQLQueryResolver {

    /**
     * Accessor to business data
     */
    @Autowired
    private BusinessDataRepository businessDataRepository;

    /** Token manager */
    @Autowired
    private AccessTokenManager accessTokenManager;

    /** Resolver linked to query "auth" defined in the schema. */
    public String auth(String personName) throws Exception
    {
        return this.accessTokenManager.issueToken(personName);
    }

    /**
     * Resolver linked to query "allCats" defined in the schema.
     */
    public List<Cat> allCats(boolean onlyFree, int limit) throws Exception {
        return this.businessDataRepository.findAllCats(onlyFree, limit);
    }

    /**
     * Resolver linked to query "cats" defined in the schema.
     */
    public List<Cat> cats(String namePrefix, int limit) throws Exception {
        //[VULN]: There an SQLi here because the repository using string concatenation for this query
        //Payload is:
        /*
            query sqli {
              dogs(namePrefix:"ab%' UNION ALL SELECT 50 AS ID, C.CFGVALUE AS NAME, NULL AS PERSON_ID FROM CONFIG C LIMIT ? -- ",limit: 1000){
                id,name
              }
            }
         */
        return this.businessDataRepository.findByNamePrefix(namePrefix, limit);
    }

    /**
     * Resolver linked to query "myCats" defined in the schema.
     */
    public List<Cat> myCats(String accessToken, int personid) throws Exception {
        //[VULN]: There an access control issue here because the verification of the access token do not verify that the token belong to the person passed in "personId"
        accessTokenManager.verifyToken(accessToken);
        return this.businessDataRepository.findPersonById(personid).getCats();
    }

    /**
     * Resolver linked to query "myInfo" defined in the schema.
     */
    public Person myInfo(String accessToken, int personId) throws Exception {
        //[VULN]: There an access control issue here because the verification of the access token do not verify that the token belong to the person passed in "personId"
        accessTokenManager.verifyToken(accessToken);
        return this.businessDataRepository.findPersonById(personId);
    }
}
