package eu.righettod.graphqlpoc.resolvers;

import com.coxautodev.graphql.tools.GraphQLMutationResolver;
import eu.righettod.graphqlpoc.repository.BusinessDataRepository;
import eu.righettod.graphqlpoc.security.AccessTokenManager;
import eu.righettod.graphqlpoc.types.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Resolvers for all Mutations defined in the Schema.
 */
@Component
public class Mutation implements GraphQLMutationResolver {

    /**
     * Accessor to business data
     */
    @Autowired
    private BusinessDataRepository businessDataRepository;

    /**
     * Token manager
     */
    @Autowired
    private AccessTokenManager accessTokenManager;


    /**
     * Resolver linked to mutation "associateCatToMe" defined in the schema.
     */
    public Cat associateCatToMe(String accessToken, int personId, int catId) throws Exception {
        //[VULN]: There an access control issue here because the verification of the access token do not verify that the token belong to the veterinary passed in "personId"
        accessTokenManager.verifyToken(accessToken);
        return this.businessDataRepository.associatedCatToPerson(personId, catId);
    }

    /**
     * Resolver linked to mutation "disassociateCatFromMe" defined in the schema.
     */
    public Cat disassociateCatFromMe(String accessToken, int personId, int catId) throws Exception {
        //[VULN]: There an access control issue here because the verification of the access token do not verify that the token belong to the veterinary passed in "personId"
        accessTokenManager.verifyToken(accessToken);
        return this.businessDataRepository.disassociateCatFromPerson(personId, catId);
    }


}
