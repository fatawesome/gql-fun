package eu.righettod.graphqlpoc.resolvers;

import com.coxautodev.graphql.tools.GraphQLResolver;
import eu.righettod.graphqlpoc.repository.BusinessDataRepository;
import eu.righettod.graphqlpoc.types.Cat;
import eu.righettod.graphqlpoc.types.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * Resolve Cat object from a Person object.
 */
@Component
public class PersonResolver implements GraphQLResolver<Person> {

    /**
     * Accessor to business data
     */
    @Autowired
    private BusinessDataRepository businessDataRepository;

    /**
     * Resolve the Cat instance from a Person instance
     *
     * @param v The Person instance
     * @return The list of Cat instances for a the Person instance
     * @throws Exception if any error occurs
     */
    public List<Cat> cats(Person v) throws Exception {
        Person vt = businessDataRepository.findPersonById(v.getId());
        return vt != null ? vt.getCats() : null;
    }
}
