package eu.righettod.graphqlpoc.repository;

import eu.righettod.graphqlpoc.types.Cat;
import eu.righettod.graphqlpoc.types.Person;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Represents the CRUD actions on business data at storage level.
 */
@Component
public class BusinessDataRepository {

    private Connection storageConnection;

    /**
     * Constructor in charge of made the DB connection available
     *
     * @throws Exception If DB connection cannot be opened
     */
    public BusinessDataRepository() throws Exception {
        //Load the SQLite driver
        Class.forName("org.sqlite.JDBC");
        File dbLocation = new File("target/poc.db");
        File dbSQLLocation = new File("target/db.sql");
        if(!dbLocation.getParentFile().exists()){
            dbLocation.getParentFile().mkdirs();
        }
        String url = "jdbc:sqlite:" + dbLocation.getAbsolutePath().replace('\\', '/');
        //Open the connection
        this.storageConnection = DriverManager.getConnection(url);
        //Init the DB because the driver do not allow execution on script at connection time
        InputStream is = this.getClass().getResourceAsStream("/db.sql");
        Files.copy(is,dbSQLLocation.toPath(), StandardCopyOption.REPLACE_EXISTING);
        List<String> instructions = Files.readAllLines(dbSQLLocation.toPath());
        instructions.forEach(sql -> {
            try {
                if (sql.trim().length() > 0) {
                    try (Statement stmt = this.storageConnection.createStatement()) {
                        stmt.execute(sql);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }


    /**
     * Get all cats based on filtering options passed
     *
     * @param namePrefix Prefix applied on the name of the cat
     * @param limit      Max number of record wanted
     * @return List of CatResolver object
     * @throws Exception If CatResolver data cannot be retrieved
     */
    public List<Cat> findByNamePrefix(String namePrefix, int limit) throws Exception {
        List<Cat> cats = null;
        String sqlCat = "SELECT ID, NAME, PERSON_ID FROM CAT WHERE NAME LIKE '" + namePrefix + "%' LIMIT ?";
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sqlCat)) {
            stmt.setInt(1, limit);
            try (ResultSet rst = stmt.executeQuery()) {
                while (rst.next()) {
                    if (cats == null) {
                        cats = new ArrayList<>();
                    }
                    int menad_id = rst.getInt("PERSON_ID");
                    if (menad_id != 2) {
                        cats.add(new Cat(rst.getInt("ID"), rst.getString("NAME"), this.findPersonById(rst.getInt("PERSON_ID"))));
                    }
                }
            }
        }
        return cats;
    }

    /**
     * Get all cats based on filtering options passed
     *
     * @param onlyFree Flag to indicate that we only want the cats not associated to a Person
     * @param limit    Max number of record wanted
     * @return List of CatResolver object
     * @throws Exception If CatResolver data cannot be retrieved
     */
    public List<Cat> findAllCats(boolean onlyFree, int limit) throws Exception {
        List<Cat> cats = null;
        String sqlCatAll = "SELECT ID, NAME, PERSON_ID FROM CAT LIMIT ?";
        String sqlCatOnlyFree = "SELECT ID, NAME FROM CAT WHERE PERSON_ID IS NULL LIMIT ?";
        String sqlCat = onlyFree ? sqlCatOnlyFree : sqlCatAll;
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sqlCat)) {
            stmt.setInt(1, limit);
            try (ResultSet rst = stmt.executeQuery()) {
                while (rst.next()) {
                    int menad_check = rst.getInt("PERSON_ID");
                    if (menad_check != 2) {
                        if (cats == null) {
                            cats = new ArrayList<>();
                        }
                        if (onlyFree) {
                            cats.add(new Cat(rst.getInt("ID"), rst.getString("NAME"), null));
                        } else {
                            cats.add(new Cat(rst.getInt("ID"), rst.getString("NAME"), this.findPersonById(rst.getInt("PERSON_ID"))));
                        }
                    }
                }
            }
        }

        return cats;
    }

    /**
     * Get Person info base on his ID.
     *
     * @param personId Person ID
     * @return Person object
     * @throws Exception If Person cannot be found
     */
    public Person findPersonById(int personId) throws Exception {
        Person person = null;
        String sqlPerson = "SELECT ID, NAME, POPULARITY FROM PERSON WHERE ID=?";
        String sqlCat = "SELECT ID, NAME FROM CAT WHERE PERSON_ID=?";
        //Find Vet info
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sqlPerson)) {
            stmt.setInt(1, personId);
            try (ResultSet rst = stmt.executeQuery()) {
                while (rst.next()) {
                    List<Cat> cats = null;
                    String personName = rst.getString("NAME");
                    int personPopularity = rst.getInt("POPULARITY");
                    // Find person's cat list
                    try (PreparedStatement stmt2 = this.storageConnection.prepareStatement(sqlCat)) {
                        stmt2.setInt(1, personId);
                        try (ResultSet rst2 = stmt2.executeQuery()) {
                            while (rst2.next()) {
                                if (cats == null) {
                                    cats = new ArrayList<>();
                                }
                                cats.add(new Cat(rst2.getInt("ID"), rst2.getString("NAME"), null));
                            }
                        }
                    }
                    person = new Person(personId, personName, personPopularity, cats);
                }
            }

        }
        //Affect veto for each cat
        if (person != null && person.getCats() != null) {
            List<Cat> l = person.getCats();
            for (Cat d : l) {
                d.setPerson(person);
            }
        }

        return person;
    }

    /**
     * Get CatResolver info base on his ID
     *
     * @param catId CatResolver ID
     * @return CatResolver object
     * @throws Exception If CatResolver cannot be found
     */
    public Cat findCatById(int catId) throws Exception {
        Cat d = null;
        String sql = "SELECT ID, NAME, PERSON_ID FROM CAT WHERE ID = ?";
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
            stmt.setInt(1, catId);
            try (ResultSet rst = stmt.executeQuery()) {
                while (rst.next()) {
                    d = new Cat(rst.getInt("ID"), rst.getString("NAME"), this.findPersonById(rst.getInt("PERSON_ID")));
                }
            }
        }
        return d;
    }

    /**
     * Associated a cat to a person if the cat is free
     *
     * @param personId Person ID
     * @param catId CatResolver ID
     * @return a CatResolver object updated if the association has been made, NULL otherwise.
     * @throws Exception If an error occur during the association
     */
    public Cat associatedCatToPerson(int personId, int catId) throws Exception {
        Cat d = null;
        //Verify is the specified CatResolver is already associated
        String sql = "SELECT COUNT(ID) FROM CAT WHERE PERSON_ID IS NOT NULL and ID = ?";
        boolean isAssociated;
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
            stmt.setInt(1, catId);
            try (ResultSet rst = stmt.executeQuery()) {
                isAssociated = rst.getInt(1) > 0;
            }
        }
        //Associated it if it's free
        if (!isAssociated) {
            sql = "UPDATE CAT SET PERSON_ID = ? WHERE ID = ?";
            try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
                stmt.setInt(1, personId);
                stmt.setInt(2, catId);
                int count = stmt.executeUpdate();
                if (count != 1) {
                    throw new Exception("No data updated!");
                }
            }
            d = findCatById(catId);
        }

        return d;
    }

    /**
     * Disassociated a cat from a person if the cat is associated to the person specified
     *
     * @param personId Person ID
     * @param catId        CatResolver ID
     * @return a CatResolver object updated if the association has been made, NULL otherwise.
     * @throws Exception If an error occur during the association
     */
    public Cat disassociateCatFromPerson(int personId, int catId) throws Exception {
        Cat d = null;
        //Verify is the specified CatResolver is already associated to the specified person
        String sql = "SELECT COUNT(ID) FROM CAT WHERE PERSON_ID = ? and ID = ?";
        boolean isAssociated;
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
            stmt.setInt(1, personId);
            stmt.setInt(2, catId);
            try (ResultSet rst = stmt.executeQuery()) {
                isAssociated = rst.getInt(1) > 0;
            }
        }
        //Disassociated it if it's associated
        if (!isAssociated) {
            sql = "UPDATE CAT SET PERSON_ID = NULL WHERE PERSON_ID = ? AND ID = ?";
            try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
                stmt.setInt(1, personId);
                stmt.setInt(2, catId);
                int count = stmt.executeUpdate();
                if (count != 1) {
                    throw new Exception("No data updated!");
                }
            }
            d = findCatById(catId);
        }

        return d;
    }

    /**
     * Load configuration parameter
     *
     * @param key Identification key of the parameter
     * @return The config value
     * @throws Exception If any error occurs
     */
    public String loadCfgParam(String key) throws Exception {
        String v = null;
        String sql = "SELECT CFGVALUE FROM CONFIG WHERE CFGKEY = ?";
        try (PreparedStatement stmt = this.storageConnection.prepareStatement(sql)) {
            stmt.setString(1, key);
            try (ResultSet rst = stmt.executeQuery()) {
                while (rst.next()) {
                    v = rst.getString("CFGVALUE");
                }
            }
        }
        return v;
    }


}
